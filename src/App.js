import React, {useState} from 'react';

import {Input} from "@material-ui/core";

import RandomNumpadPopover from "./components/RandomNumpadPopover";

const App =  () => {

    const [inputValue, setInputValue] = useState("");

    const onChange = (value) =>{
        setInputValue(value)
    };

    return (
        <div
            style={{
                display : "flex"
            }}
        >
            <Input value={inputValue} placeholder={"Input"}/>
            <RandomNumpadPopover
                popoverAlign={"end"}
                popoverPosition={"bottom"}
                onInputChange={onChange}
                contentPopup={<button > Click </button>}
            />
        </div>
    )
};

export default App;
