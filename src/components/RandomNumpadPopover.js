import React, {useState} from 'react';
import PropTypes from 'prop-types';

import Popover, { ArrowContainer } from 'react-tiny-popover'

import RandomNumpadContainer from "./RandomNumpadContainer";

const RandomNumpadPopover = (props) =>{

    const [popover, setPopover] = useState(false);

    const onPopoverOpen = () =>{
        if(popover !== true){
            setPopover(true)
        }
    };

    const onPopoverClose = () =>{
        setPopover(false)
    };

    return (
        <Popover
            isOpen={popover}
            align={props.popoverAlign}
            position={props.popoverPosition}
            content={({position, targetRect, popoverRect })=>(
                    <ArrowContainer
                        position={position}
                        targetRect={targetRect}
                        popoverRect={popoverRect}
                        arrowColor={"#d5d1d1"}
                        arrowSize={10}
                        arrowStyle={{ opacity: 0.7 }}
                    >
                        <RandomNumpadContainer onChange={props.onInputChange} onClose={onPopoverClose} />
                    </ArrowContainer>
            )}
            onClickOutside={onPopoverClose}
        >
            <div
                className={"popup-container"}
                onClick={onPopoverOpen}
            >
                {props.contentPopup}
            </div>
        </Popover>
    );
};

RandomNumpadPopover.propTypes = {
    contentPopup: PropTypes.element,
    popoverAlign : PropTypes.string,
    popoverPosition : PropTypes.string,
    onInputChange : PropTypes.func
};


export default RandomNumpadPopover;
