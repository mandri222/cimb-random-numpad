import React, {useState} from 'react';
import PropTypes from 'prop-types';

import { Icon } from 'react-icons-kit'
import {ic_backspace} from 'react-icons-kit/md/ic_backspace'

import shuffle from "../utilities/shuffle";

const RandomNumpadContainer = (props) => {

    const initialArray = [0,1,2,3,4,5,6,7,8,9];
    const [data] = useState(shuffle(initialArray));
    const [input, setInput] = useState("");

    const addNumber= (inputUser) =>{
        let concatUser = input.concat(inputUser);
        setInput(concatUser);
        props.onChange(concatUser);
    };

    const clear = () =>{
        setInput("");
        props.onChange("");
    };

    const backspace = () =>{
        let substr = input.substr(0, input.length-1);
        setInput(substr);
        props.onChange(substr);
    };

    let j = 0;
    const numpadAction = ["",props.onClose, clear, backspace];
    const numpadIcon = ["", "X", "Clear", <Icon style={{paddingBottom : 3}} icon={ic_backspace} />];
    const numpadStyle = ["", "close", "delete", "backspace"];

    return (
        <div
            className={"numpad-popup"}
        >
            {
                data.map((data, i) => {
                    if(i === 2 || i === 5 || i === 8 ){
                        j++;
                        return  <div key={i}>
                            <button onClick={()=>(addNumber(data))} className={"buttonWidth bShadow-9"}>{data}</button>
                            <button  onClick={numpadAction[j]} className={`action-button bShadow-9 ${numpadStyle[j]}`} >{numpadIcon[j]}</button>
                        </div>
                    } else {
                        if(i === 9){
                            return  <div key={i}>
                                <button style={{
                                    visibility : "hidden"
                                }} className={"buttonWidth"}>""</button>
                                <button onClick={()=>(addNumber(data))} className={"buttonWidth bShadow-9"}>{data}</button>
                            </div>
                        }else {
                            return  <button key={i} onClick={()=>(addNumber(data))} className={"buttonWidth bShadow-9"}>{data}</button>
                        }
                    }
                })
            }
        </div>
    );
};


RandomNumpadContainer.propTypes = {
    onClose : PropTypes.func
};

export default RandomNumpadContainer;
